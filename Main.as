﻿package  {
	import flash.display.Sprite;
	import flash.events.*;
    import flash.display.*;
	import flash.net.*;
	import utilities.*;
	import utilities.Managers.*;
	
	public class Main extends Sprite{
		public static var STAGE:Stage;
		public static var gameManager:GameManager;
		public static var uiManager:UIManager;
		public static var mouseInputManager:MouseInputManager;
		public static var resourceLoader:ResourceLoader;
		public static var webApiHandler: WebApiHandler;
		
		public function Main() {
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init, false, 0, true);
		}

		private function init(e:Event=null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			STAGE=stage;
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode=StageScaleMode.NO_SCALE;
			resourceLoader = new ResourceLoader(stage); 
			resourceLoader.init();
			resourceLoader.addEventListener(Event.COMPLETE, initManagers);
		}
		
		private function initManagers(e:Event):void
		{
			mouseInputManager = new MouseInputManager(stage);
			uiManager = new UIManager(stage);
			gameManager = new GameManager(stage);
			webApiHandler = new WebApiHandler();
			gameManager.initControllers();
			
		}
	}
	
}
