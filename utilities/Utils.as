﻿package utilities {
	
	public class Utils {
		public static function getFileName(fullPath: String) : String
		{
			var fSlash: int = fullPath.lastIndexOf("/");
			var bSlash: int = fullPath.lastIndexOf("\\");
			var slashIndex: int = fSlash > bSlash ? fSlash : bSlash;
			return fullPath.substr(slashIndex + 1);
		}
		
		public static function getFileNameWithoutExtension(fullPath: String) : String
		{
			var fSlash: int = fullPath.lastIndexOf("/");
			var bSlash: int = fullPath.lastIndexOf("\\");
			var slashIndex: int = fSlash > bSlash ? fSlash : bSlash;
			var filename:String = fullPath.substr(slashIndex + 1);
			var extensionIndex:Number = filename.lastIndexOf( '.' );
			var extensionless:String = filename.substr( 0, extensionIndex ); 
			return extensionless;
		}
		
		public static function getExtension($url:String):String
		{
			var extension:String = $url.substring($url.lastIndexOf(".")+1, $url.length);
			return extension;
		}
		
		public static function randomRange(minNum:Number, maxNum:Number):Number 
		{
			return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}
	}
}
