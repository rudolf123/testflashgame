﻿package utilities.Managers {
	
	import flash.display.Stage;
	import flash.text.TextField;
	import core.GameObject;
		
	public class UIManager {
		private var stage:Stage;
		private var txtEnergy:TextField;
		private var txtItems:TextField;
		
		public function UIManager(stage:Stage) {
			this.stage = stage;
			txtEnergy = new TextField();
			txtEnergy.text = "Energy";
			txtItems = new TextField();
			txtItems.x = 0;
			stage.addChild(txtEnergy);
			stage.addChild(txtItems);
		}
		
		public function changeEnergy(energyCount:int){
			stage.setChildIndex(txtEnergy,stage.numChildren - 1);
			txtEnergy.text = "Энергия = " + energyCount;
		}
		
		public function updateItemsList(data:String){
			stage.setChildIndex(txtItems,stage.numChildren - 1);
			txtItems.text = data;
		}

	}
	
}
