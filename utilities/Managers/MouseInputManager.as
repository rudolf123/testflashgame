﻿package utilities.Managers{
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.display.Stage;
	import core.GameObject;
	import interfaces.IDraggable;
	import interfaces.IClikable;
	import flash.events.EventDispatcher;
	
	public class MouseInputManager extends EventDispatcher{
		
		public static var isDragging:Boolean = false;
		private var mouseOffSet:Point = new Point;
		private var objectOffSet:Point = new Point;
		private var delta:Point = new Point;
		private var oldMouse:Point = new Point;
		private var maximumEase:Number = 30;
		private var lastClickedGameObject:GameObject;
		private var lastClickedCoordinates:Point = new Point;
		private var stage:Stage;
		private var needToDispatchClick:Boolean = false;
		private var clickedGameObject:IClikable;
		private var localCoordinatesOfClickedObject:Point;
		
		public function MouseInputManager(stage:Stage):void
		{
			this.stage = stage;
			stage.addEventListener(MouseEvent.MOUSE_DOWN, checkForMouseCollidingObject);
			stage.addEventListener(MouseEvent.MOUSE_UP, dispatchClickOnObject);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, checkForMouseMove);
		}
			
		public function getMouseCoordinates():Point
		{
			var mousePoint:Point = new Point(stage.mouseX, stage.mouseY);
			return mousePoint;
		}

		public function dragObject(e:Event):void 
		{
			if (isDragging) 
			{
				var mousePoint:Point = new Point();
				mousePoint = getMouseCoordinates();
				delta.x = (mousePoint.x - oldMouse.x);
				delta.y = (mousePoint.y - oldMouse.y);
				var x:int = mousePoint.x + objectOffSet.x - mouseOffSet.x;
				var y:int = mousePoint.y + objectOffSet.y - mouseOffSet.y;
				var objectToDrag:IDraggable = lastClickedGameObject as IDraggable;
				objectToDrag.drag(new Point(x, y)) as IDraggable;
				
			}else if (!isDragging) {
				delta.x *= .8;
				delta.y *= .8;
				if (delta.x > maximumEase) {
					delta.x = maximumEase;
				}
				if (delta.y > maximumEase) {
					delta.y = maximumEase;
				}
				if (delta.x < -maximumEase) {
					delta.x = -maximumEase;
				}
				if (delta.y < -maximumEase) {
					delta.y = -maximumEase;
				}
				if (delta.x < .05 && delta.x > -.05) {
					delta.x = 0;
				}
				if (delta.y < .05 && delta.y > -.05) {
					delta.y = 0;
				}
				if (delta.x == 0 && delta.y == 0 ) {
					stage.removeEventListener(Event.ENTER_FRAME, dragObject);
				}
				var x:int = lastClickedGameObject.x + delta.x;
				var y:int = lastClickedGameObject.y + delta.y;
				var objectToDrag:IDraggable = lastClickedGameObject as IDraggable;
				objectToDrag.drag(new Point(x, y)) as IDraggable;
			}
			oldMouse = getMouseCoordinates();
		}
		
		public function stopDragObject(event:MouseEvent):void 
		{
			isDragging = false;
			(lastClickedGameObject as IDraggable).onStopDragging();
			stage.removeEventListener(MouseEvent.MOUSE_UP, stopDragObject);
		}
		
		public function getLastClickedCoordinates():Point 
		{
			return lastClickedCoordinates;
		}
		
		private function checkForMouseCollidingObject(event:MouseEvent)
		{
			var mousePoint:Point = new Point();
			mousePoint = getMouseCoordinates();
			var sceneGameObjects:Array = Main.gameManager.getListOfGameObjects();
			for each(var gameObject:GameObject in sceneGameObjects) {
				if (gameObject.hitTestPoint(mousePoint.x,mousePoint.y)) {
					trace(gameObject.name);
					if (gameObject is IDraggable)
					{
						lastClickedGameObject = gameObject;
						startDrag(gameObject as IDraggable);
					}
					if (gameObject is IClikable)
					{
						needToDispatchClick = true;
						clickedGameObject = gameObject as IClikable;
						localCoordinatesOfClickedObject = gameObject.globalToLocal(mousePoint);
					}
				}
			}
		}
		
		private function dispatchClickOnObject(e:MouseEvent):void
        {				
			if(needToDispatchClick){				
				var ev:MouseEvent  = new MouseEvent(MouseEvent.CLICK);
				ev.localX = localCoordinatesOfClickedObject.x;
				ev.localY = localCoordinatesOfClickedObject.y;
				clickedGameObject.onClickAction(ev);
			}
        }
		
		private function checkForMouseMove(event:MouseEvent)
		{
			needToDispatchClick = false;
		}
		
		private function startDrag(object:IDraggable):void 
		{
			isDragging = true;
			mouseOffSet = getMouseCoordinates();
			objectOffSet = object.GetCoordinates();
			stage.addEventListener(Event.ENTER_FRAME, dragObject);
			stage.addEventListener(MouseEvent.MOUSE_UP, stopDragObject);
		}
	}
}

