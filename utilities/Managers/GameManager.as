﻿package  utilities.Managers{
	import flash.display.Stage;
	import core.GameObject;
	import utilities.Controllers.*;
	
	public class GameManager {
		private var stage:Stage;
		private var gameObjects:Array;
		private var roomController:RoomController;
		private var playerController:PlayerController;
		private var garbageController:GarbageController;
		private var gameObjectsCounter:int;
		
		public function GameManager(stage:Stage) 
		{
			this.stage = stage;
			gameObjects = new Array();
			gameObjectsCounter = 0;
		}
		
		public function initControllers()
		{
			roomController = new RoomController();
		    playerController = new PlayerController(roomController.getCurrentRoom());
			garbageController = new GarbageController(roomController.getCurrentRoom(), playerController);
		}
		
		public function addGameObject(gameObject:GameObject):GameObject
		{
			gameObjectsCounter++;
			gameObject.name += gameObjectsCounter.toString();
			stage.addChild(gameObject);
			gameObjects.push(gameObject);
			
			return gameObject;
		}
		
		public function removeGameObject(gameObject:GameObject):void
		{
			gameObject.parent.removeChild(gameObject);
		}
		
		public function addGameObjectWithParent(gameObject:GameObject, parent:GameObject):GameObject
		{
			parent.addChild(gameObject);
			gameObjects.push(gameObject);
			
			return gameObject;
		}
		
		public function getListOfGameObjects()
		{
			return gameObjects;
		}
	}
	
}
