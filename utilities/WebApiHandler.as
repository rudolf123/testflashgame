﻿package utilities {
	import com.adobe.serialization.json.JSON;
	import flash.net.*;
	import flash.events.*;
	
	public class WebApiHandler {
		var loader:URLLoader;
		var apiRoot:String = "http://cv57083.tmweb.ru/api/";
		public function WebApiHandler() {
			
		}
		
		public function checkPlayerStatus(onComplete:Function){
			makeRequest("usersstatus/1","", onComplete, URLRequestMethod.GET);
		}
		
		public function createPlayerStatus(jsonData:String, onComplete:Function){
			makeRequest("usersstatus", jsonData, onComplete, URLRequestMethod.POST);
		}
		
		public function updatePlayerStatus(jsonData:String, onComplete:Function){
			makeRequest("updateuserstatus", jsonData, onComplete, URLRequestMethod.POST);
		}
		
		public function checkRoomStatus(onComplete:Function){
			makeRequest("roomstatus/1","", onComplete, URLRequestMethod.GET);
		}
		
		public function createRoomStatus(jsonData:String, onComplete:Function){
			makeRequest("roomstatus", jsonData, onComplete, URLRequestMethod.POST);
		}
		
		public function updateRoomStatus(jsonData:String, onComplete:Function){
			makeRequest("updateroomstatus", jsonData, onComplete, URLRequestMethod.POST);
		}
		
		public function checkPlayerItems(onComplete:Function){
			makeRequest("useritems/1","", onComplete, URLRequestMethod.GET);
		}
		
		public function updatePlayerItems(jsonData:String, onComplete:Function){
			makeRequest("useritems",jsonData, onComplete, URLRequestMethod.POST);
		}
		
		private function makeRequest(url:String, jsonData:String, onComplete:Function, method:String = URLRequestMethod.GET )
		{
			var request:URLRequest=new URLRequest();
			request.url = apiRoot + url;
			request.requestHeaders = [new URLRequestHeader("Content-Type", "application/json")];
			request.method = method;
			request.data = jsonData;
			loader=new URLLoader();
			loader.addEventListener(Event.COMPLETE, onComplete);
			loader.load(request);
		}
	}	
}
