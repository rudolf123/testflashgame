﻿package  utilities{
	import flash.display.Stage;
	import flash.display.Loader;
	import flash.net.URLRequest;
	import flash.system.LoaderContext;
	import flash.system.SecurityDomain;
	import flash.system.ApplicationDomain;
	import flash.events.*;
	import flash.filesystem.File;
	import core.*
	
	public class ResourceLoader extends EventDispatcher{
		private var stage:Stage;
		private var context:LoaderContext;
		private var loader:Loader;
		private var onCompleteLoaded:Function;
		private var loadingQueue:Array;
		
		public function ResourceLoader(stage:Stage) {
			this.stage = stage;
			context = new LoaderContext(); 
			loader = new Loader();
			loadingQueue = new Array();
			context.applicationDomain = ApplicationDomain.currentDomain;
		}
		
		public function init()
		{
			loadCollectionsResources();
			loadGarbageResources();
			loadBackgroundResources();
			loadPlayerResources();
		}
		
		public function loadAllResourcesAtPath(path:String, fileExt:String, type:String)
		{
			var desktop:File = File.applicationDirectory.resolvePath(path);
			var files:Array = desktop.getDirectoryListing();

			for (var i:uint = 0; i < files.length; i++)
			{
				if (Utils.getExtension(files[i].nativePath) == fileExt)
				{
					var loader:Loader = new Loader();
					var urlReq:URLRequest = new URLRequest(path + files[i].name); 
					loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onCompleteAssetLoad(type)); 
					loader.load(urlReq, context);
					loadingQueue[files[i].name] = "unloaded";
				}
			}
		}
		
		public function loadCollectionsResources()
		{
			loadAllResourcesAtPath(Resources.collectionsPath, "swf", "collections");
		}
		
		public function loadGarbageResources()
		{
			loadAllResourcesAtPath(Resources.garbagePath, "swf", "garbage");
		}
		
		public function loadBackgroundResources()
		{
			loadAllResourcesAtPath(Resources.backgroundsPath, "swf", "background");
		}
		
		public function loadPlayerResources()
		{
			loadAllResourcesAtPath(Resources.playerPath, "swf", "player");
		}
		
		private function onCompleteAssetLoad(assetType:String):Function {
			return function(e:Event):void {
				trace(Utils.getFileNameWithoutExtension(e.target.url) + " has been loaded!");
				var item:Object = {};
				item.objectClassName = e.target.applicationDomain.getDefinition(Utils.getFileNameWithoutExtension(e.target.url)) as Class
				item.objectName = Utils.getFileNameWithoutExtension(e.target.url);
				item.objectType = assetType;
				Resources.loadedResources[item.objectName] = item;
				loadingQueue[Utils.getFileName(e.target.url)] = "loaded";
				if(checkIfQuqeueEmpty())
					dispatch();
			};
		}
		
		private function checkIfQuqeueEmpty():Boolean{
			for each (var o:* in loadingQueue)
			{
				if (o == "unloaded")
					return false;
			}
			return true;
		}
		
		private function dispatch():void
        {
            dispatchEvent(new Event(Event.COMPLETE));
        }
	}
	
}
