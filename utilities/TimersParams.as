﻿package utilities {
	
	public class TimersParams {
		public static const ENERGY_RESTORE:Number = 20000;
		public static const GARBAGE_SPAWN:Number = 10000;
		public static const COLLECTION_ITEM_VISIBLE:Number = 3000;	
		
	}
	
}
