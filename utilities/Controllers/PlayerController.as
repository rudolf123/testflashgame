﻿package utilities.Controllers {
	import flash.geom.Point;
	import flash.display.MovieClip;
	import flash.events.*;
	import core.*;
	import utilities.*;
	
	public class PlayerController extends EventDispatcher{	
		private var player:Player;
		private var initPlayerPosition:Point;
		private const moveSpeed:Number = 15;
		private var targetX:Number = 0;
		private var currentRoom:Room;
		private var currentGarbageItemToClean:GarbageItem;
		
		public function PlayerController(room:Room) 
		{
			player = new Player("PlayerSprite");
			currentRoom = room;
			Main.gameManager.addGameObjectWithParent(player, room);
			currentRoom.addClicklListener(movePlayer);
			player.y = 200;
			Main.STAGE.addEventListener(Event.ENTER_FRAME, checkPlayerPositionInScreen);
			Main.webApiHandler.checkPlayerStatus(onCompleteCheckPlayerStatus);
		}
		
		private function onCompleteCheckPlayerStatus(e:Event)
		{
			var recievedObject = JSON.parse(e.target.data);
			if(recievedObject.position_x == null)
				createPlayerStatusAtServer();
			else
			{
				player.x = recievedObject.position_x;
				player.setEnergy(recievedObject.energy);
			}
		}
		
		private function createPlayerStatusAtServer()
		{
			var data:String = JSON.stringify({"user_id":"1",
						  "position_x":player.x,
						  "energy":player.getEnergyCount()});
			Main.webApiHandler.createPlayerStatus(data, onCompleteCreatePlayerAtServer);
		}
		
		private function onCompleteCreatePlayerAtServer(e:Event){
			
		}
		
		private function onCompleteUpdatePlayerAtServer(e:Event){
			
		}
		
		private function checkPlayerPositionInScreen(e:Event){
			var playerGlobalPos:Point  = player.parent.localToGlobal(player.GetCoordinates());
			if (playerGlobalPos.x < 0)
				movePlayer( player.parent.globalToLocal(new Point(0,0)));
			if (playerGlobalPos.x > Main.STAGE.nativeWindow.width)
				movePlayer( player.parent.globalToLocal(new Point(Main.STAGE.nativeWindow.width-200,0)));
		}
		
		public function cleanGarbage(item:GarbageItem){
			if (item == currentGarbageItemToClean)
				return;
			if (player.CURRENT_STATE == PlayerStates.STATE_CLEANING)
				return;
				
			if ((player.getEnergyCount() - item.getEnergyCost()) < 0)
				return;
				
			player.CURRENT_STATE = PlayerStates.STATE_CLEANING;
			targetX = item.x;
			currentGarbageItemToClean = item;
			Main.STAGE.addEventListener(Event.ENTER_FRAME, updatePlayerPosition);
		}
		
		private function movePlayer(clickedCoords:Point):void 
		{
			if (player.CURRENT_STATE == PlayerStates.STATE_CLEANING)
				return;
				
			player.CURRENT_STATE = PlayerStates.STATE_MOVING;
			targetX = clickedCoords.x;
			Main.STAGE.addEventListener(Event.ENTER_FRAME, updatePlayerPosition);
		}
		
		private function updatePlayerPosition(e:Event):void 
		{
			if (Math.abs(targetX - player.x) < moveSpeed) 
			{
				player.x = targetX;
				var data:String = JSON.stringify({"user_id":"1",
												  "position_x":player.x,
												  "energy":player.getEnergyCount()});
				Main.webApiHandler.updatePlayerStatus(data, onCompleteUpdatePlayerAtServer);
				
				Main.STAGE.removeEventListener(Event.ENTER_FRAME, updatePlayerPosition);
				if (player.CURRENT_STATE == PlayerStates.STATE_CLEANING)
				{
					runCleaning();
				}
			} 
			else if (targetX > player.x) 
			{
				player.x += moveSpeed;
			} 
			else 
			{
				player.x -= moveSpeed;
			}
		}
		
		private function runCleaning()
		{
			player.runClean();
			player.takeEnegry();
			trace("runCleaning " + currentGarbageItemToClean.name );
			player.addEventListener(Event.COMPLETE, onCleanComplete);			
		}
		
		private function onCleanComplete(e:Event)
		{
			currentGarbageItemToClean.remove();
			player.CURRENT_STATE = PlayerStates.STATE_IDLE;
		}
	}
	
}
