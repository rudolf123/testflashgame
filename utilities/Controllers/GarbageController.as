﻿package utilities.Controllers {
	import flash.utils.Timer;
	import flash.events.*;
	import flash.geom.Point;
	import core.*;
	import utilities.*;
	
	public class GarbageController {
		private var timer:Timer = new Timer(TimersParams.GARBAGE_SPAWN);
		private var garbageResources:Array;
		private var collectionResources:Array;
		private var currentRoom:Room;
		private var playerController:PlayerController;
		
		public function GarbageController(room:Room, playerController:PlayerController) 
		{
			this.playerController = playerController;
			timer.addEventListener(TimerEvent.TIMER, spawnGarbage);
			garbageResources = Resources.getResourcesByType("garbage");
			collectionResources = Resources.getResourcesByType("collections");
			currentRoom = room;
			timer.start();
			for(var i:int = 0; i < Utils.randomRange(3,10);i++)
				spawnGarbage(null);
			Main.webApiHandler.checkPlayerItems(onCompleteCheckPlayerItems);
		}
		
		private function onCompleteCheckPlayerItems(e:Event)
		{
			var recievedObject = JSON.parse(e.target.data);
			if(recievedObject.position_x != null)
			{
				Main.uiManager.updateItemsList(e.target.data);
			}
		}
		
		private function spawnGarbage(e:TimerEvent)
		{
			var randomGarbageObject:Object = garbageResources[Utils.randomRange(0, garbageResources.length-1)];
			var item:GarbageItem = new GarbageItem(randomGarbageObject.objectName);
			item.setCoordinates(new Point(Utils.randomRange(200, currentRoom.width-200),370));
			Main.gameManager.addGameObjectWithParent(item, currentRoom);
			item.addClicklListener(playerController.cleanGarbage);
			item.addRemoveListener(onGarbageItemRemove);
		}
		
		private function onGarbageItemRemove(item:GarbageItem)
		{
			spawnCollectionItem(item);
			Main.gameManager.removeGameObject(item);
			
		}
		
		private function spawnCollectionItem(garbageItem:GarbageItem)
		{
			var randomItemObject:Object = collectionResources[Utils.randomRange(0, collectionResources.length-1)];
			var item:CollectionItem = new CollectionItem(randomItemObject.objectName);
			item.setCoordinates(garbageItem.GetCoordinates());
			Main.gameManager.addGameObjectWithParent(item, currentRoom);
			item.collect();
		}
	}
	
}
