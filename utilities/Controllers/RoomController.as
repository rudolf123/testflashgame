﻿package utilities.Controllers {
	import flash.events.Event;
    import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.display.MovieClip;
	import core.Room;
	
	public class RoomController {
		private var currentRoomId:int;
		private var currentRoom:Room;
		
		public function RoomController() 
		{
			currentRoom = new Room("BigHome_Garbage_Server");
			Main.gameManager.addGameObject(currentRoom);
			Main.webApiHandler.checkRoomStatus(onCompleteCheckRoomStatus);
		}
		
		private function onCompleteCheckRoomStatus(e:Event)
		{
			var recievedObject = JSON.parse(e.target.data);
			if(recievedObject.offset == null)
				createRoomStatusAtServer();
			else
			{
				currentRoom.setOffset(recievedObject.offset);
			}
		}
		
		private function createRoomStatusAtServer()
		{
			var data:String = JSON.stringify({"room_id":"1",
						  "user_id":"1",
						  "offset":"0"});
			Main.webApiHandler.createRoomStatus(data, onCompleteCreateRoomAtServer);
		}
		
		private function onCompleteCreateRoomAtServer(e:Event){
			
		}
		
		public function getCurrentRoom():Room
		{
			return currentRoom;
		}
	}
	
}
