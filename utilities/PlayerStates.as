﻿package utilities {
	
	public class PlayerStates {

		public static const STATE_IDLE:String = "IDLE";
		public static const STATE_MOVING:String = "MOVING";
		public static const STATE_COLLECTING:String = "COLLECTING";
		public static const STATE_CLEANING:String = "CLEANING";

	}
	
}
