﻿package core{
	
	public class Resources {
		public static var assetsPath:String = "assets/";
		public static var collectionsPath:String = assetsPath + "collections/";
		public static var garbagePath:String = assetsPath + "garbage/";
		public static var backgroundsPath:String = assetsPath + "rooms/";
		public static var playerPath:String = assetsPath + "player/";
		
		public static var loadedResources:Array = new Array();
		public static var garbageCollectionsObject:Object = new Object();
		
		public static function getResourceByName(resourceName:String): Object
		{
			for each(var row:* in loadedResources)
				if(row.objectName == resourceName)
					return row;
			return null;
		}
		
		public static function getResourcesByType(resourceType:String): Array
		{
			
			var objects:Array = new Array();
			for each(var row:* in loadedResources)
			{
				if(row.objectType == resourceType)
					objects.push(row);
			
			}
			return objects;
		}
	}
	
}
