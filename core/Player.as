﻿package  core{
	
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.geom.Point;
	import flash.utils.Timer;
	import utilities.*;
	
	import core.GameObject;
	
	public class Player extends GameObject 
	{
		public var CURRENT_STATE:String;
		private var energy:int = 10;
		private var broom:Broom;
		private var energyRestoreTimer:Timer;
		
		public function Player(assetName:String) 
		{
			super(assetName);
			loadGraphicFromResource(assetName);
			CURRENT_STATE = PlayerStates.STATE_IDLE;
			name = assetName;
			
			broom = new Broom("BroomSprite");
			addChild(broom);
			broom.visible = false;
			Main.uiManager.changeEnergy(energy);
			
			energyRestoreTimer = new Timer(TimersParams.ENERGY_RESTORE);
			energyRestoreTimer.addEventListener(TimerEvent.TIMER, restoreEnergy);
			energyRestoreTimer.start();
		}
		
		public function getEnergyCount()
		{
			return energy;
		}
		
		public function setEnergy(amount:int)
		{
			energy = amount;
			Main.uiManager.changeEnergy(energy);
		}
		
		public function takeEnegry(amount:int = 1)
		{
			if ((energy - amount) >= 0)
				energy -= amount;
			Main.uiManager.changeEnergy(energy);
		}
		
		public function runClean()
		{
			broom.visible = true;
			broom.runClean();
			broom.addEventListener(Event.COMPLETE, onCleanComplete);
		}
		
		private function restoreEnergy(e:TimerEvent){
			if(energy == 10)
				return;
				
			energy++;
			Main.uiManager.changeEnergy(energy);
		}

		private function onCleanComplete(e:Event){
			broom.visible = false;
			dispatchEvent(new Event(Event.COMPLETE));
		}		
	}
	
}
