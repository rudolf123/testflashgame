﻿package core{
	import flash.utils.Timer;
	import flash.events.*;
	import core.GameObject;

	
	public class Broom extends GameObject{
		private var animationTimer:Timer;
		private var maxY:int = 20;
		private var minY:int = -20;
		private var moveTo:String = "Up";
		
		public function Broom(assetName:String) {
			super(assetName);
			loadGraphicFromResource(assetName);
			name = assetName;
		}
		
		public function runClean()
		{
			animationTimer = new Timer(100, 20);
			animationTimer.start();
			animationTimer.addEventListener(TimerEvent.TIMER, startAnimate);
			animationTimer.addEventListener(TimerEvent.TIMER_COMPLETE, stopAnimate);
		}
		
		private function startAnimate(e:TimerEvent){
			this.addEventListener(Event.ENTER_FRAME, moveUpAndDown);
		}
		
		private function moveUpAndDown(e:Event)
		{
			if(moveTo == "Up")
			{
				e.target.y += 10;
				if(e.target.y > maxY)
					moveTo = "Down";
			} else if (moveTo == "Down")
			{
				e.target.y -= 10;
				if(e.target.y < minY)
					moveTo = "Up";
			}
		}
		
		private function stopAnimate(e:TimerEvent){
			this.removeEventListener(Event.ENTER_FRAME, moveUpAndDown);
			dispatchComplete();
		}
		
		private function dispatchComplete():void
		{
            dispatchEvent(new Event(Event.COMPLETE));
		}

	}
	
}
