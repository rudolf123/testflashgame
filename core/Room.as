﻿package core {
	import core.GameObject;
	import interfaces.IDraggable;
	import interfaces.IClikable;
	import flash.geom.Point;
	import flash.system.Capabilities;
	import flash.events.MouseEvent;
	import flash.events.Event;

	public class Room extends GameObject implements IDraggable, IClikable {
		private var offset:int = 0;
		private var lastClickedCoordinates:Point;
		private var clickListeners:Array = new Array();
		
		public function Room(roomName: String) 
		{
			super(roomName);
			loadGraphicFromResource(roomName);
			this.x = offset;
			name = roomName;
		}
		
		public function drag(destination:Point):void
		{
			if (destination.x < 0)
				if (destination.x > -this.width + stage.nativeWindow.width)
				{
					this.x = destination.x;
					offset = this.x;
				}
		}
		
		public function onStopDragging():void
		{
			var data:String = JSON.stringify({"room_id":"1",
			  "user_id":"1",
			  "offset":offset});
			Main.webApiHandler.updateRoomStatus(data, onCompleteUpdateRoomStatus);
		}
		
		private function onCompleteUpdateRoomStatus(e:Event){
			
		}
		
		public override function GetCoordinates():Point
		{
			return super.GetCoordinates();
		}
		
		public function setOffset(offset:int)
		{
			this.x = offset;
		}
		
		public function addClicklListener(func:Function)
		{
			clickListeners.push(func);
		}
		
		public function getLastClickedCoordinates():Point
		{
			return lastClickedCoordinates;
		}
		
		public function onClickAction(e:MouseEvent)
		{
			lastClickedCoordinates = new Point(e.localX, e.localY);
			dispatch(e);
		}
		
		private function dispatch(e:MouseEvent):void
        {
			for (var i:uint = 0; i < clickListeners.length; i++)
			{
				clickListeners[i](new Point(e.localX, e.localY));
			}
        }
		

	}
	
}
