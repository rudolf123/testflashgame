﻿package  core{
	import flash.display.MovieClip;;
	import flash.geom.Point;
	import flash.display.DisplayObject;
    import flash.display.Sprite;
	import flash.events.*;
	
	public class GameObject extends MovieClip{
		protected var goName:String;
		protected var goParent:GameObject;
		protected var clip:MovieClip;
		
		public function GameObject(name:String){
			super();
			goName = name;
		}
		
		public function setName(name:String)
		{
			goName = name;
		}
		
		public function SetParent(parent:GameObject)
		{
			parent.addChild(this);
		}
		
		public function loadGraphicFromResource(resourceName:String){
			var ClassDefinition:Class = Resources.loadedResources[resourceName].objectClassName;
			clip = new ClassDefinition();
			addChild(clip);
		}
		
		public function GetCoordinates():Point{
			var coordPoint:Point = new Point(this.x,this.y);
			return coordPoint;
		}
		
		public function setCoordinates(point:Point):void{
			this.x = point.x;
			this.y = point.y;
		}

	}
	
}
