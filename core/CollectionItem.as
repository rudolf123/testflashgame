﻿package core {
	import core.GameObject;
	import flash.utils.Timer;
	import flash.events.*;
	import utilities.*

	
	public class CollectionItem extends GameObject{
		private var id:int;
		private var itemName:String;
		private var animationTimer:Timer;
		private var maxScale:int = 100;
		private var minScale:int = 90;
		private var moveTo:String = "Grow";
		
		public function CollectionItem(assetName:String) {
			super(assetName);
			itemName = assetName;
			loadGraphicFromResource(assetName);
		}
		
		public function collect(){
			animationTimer = new Timer(100, TimersParams.COLLECTION_ITEM_VISIBLE/100);
			animationTimer.addEventListener(TimerEvent.TIMER, startScaleAnimation);
			animationTimer.addEventListener(TimerEvent.TIMER_COMPLETE, stopScaleAnimation);
			animationTimer.start();
			
			var data:String = JSON.stringify(
											 {"user_id":"1",
			  								  "item_id":itemName});
			Main.webApiHandler.updatePlayerItems(data, onCompleteUpdatePlayerItems);
		}
		
		private function onCompleteUpdatePlayerItems(e:Event){
			
		}
		
		private function startScaleAnimation(e:TimerEvent){
			this.addEventListener(Event.ENTER_FRAME, scaleAnimation);
		}
		
		private function startDisposeAnimation(e:TimerEvent){
			this.addEventListener(Event.ENTER_FRAME, disposeAnimation);
		}
		
		private function scaleAnimation(e:Event)
		{
			if(moveTo == "Grow")
			{
				e.target.width += 1;
				e.target.height += 1;
				if(e.target.width > maxScale)
					moveTo = "Shrink";
			} else if (moveTo == "Shrink")
			{
				e.target.width -= 1;
				e.target.height -= 1;
				if(e.target.width < minScale)
					moveTo = "Grow";
			}
		}
		
		private function disposeAnimation(e:Event)
		{
			e.target.y -= 30;
			e.target.alpha -= 0.1;
		}
		
		private function stopScaleAnimation(e:TimerEvent){
			this.removeEventListener(Event.ENTER_FRAME, scaleAnimation);
			animationTimer = new Timer(100, 20);
			animationTimer.addEventListener(TimerEvent.TIMER, startDisposeAnimation);
			animationTimer.addEventListener(TimerEvent.TIMER_COMPLETE, stopDisposeAnimation);
			animationTimer.start();
		}
		
		private function stopDisposeAnimation(e:TimerEvent){
			this.removeEventListener(Event.ENTER_FRAME, disposeAnimation);
			Main.gameManager.removeGameObject(this);
		}
	}
	
}
