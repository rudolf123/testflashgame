﻿package  core{
	import flash.geom.Point;
	import flash.events.MouseEvent;
	
	import core.GameObject;
	import interfaces.IClikable;
	import interfaces.IRemovable;

		
	public class GarbageItem extends GameObject implements IClikable, IRemovable{
		public var itemName:String;
		private var lastClickedCoordinates:Point;
		private var clickListeners:Array = new Array();
		private var removeListeners:Array = new Array();
		private var energyCost:int = 1;
		
		public function GarbageItem(assetName:String) 
		{
			super(assetName);
			itemName = assetName;
			loadGraphicFromResource(assetName);
			Main.mouseInputManager.addEventListener(MouseEvent.CLICK, onClickAction);
		}
		
		private function dispatchOnClick(e:MouseEvent):void
        {
			for (var i:uint = 0; i < clickListeners.length; i++)
				clickListeners[i](this);
        }
		
		private function dispatchOnRemove():void
        {
			for (var i:uint = 0; i < clickListeners.length; i++)
				removeListeners[i](this);
        }
		
		public function getEnergyCost()
		{
			return energyCost;
		}
		
		public function addClicklListener(func:Function)
		{
			clickListeners.push(func);
		}
		
		public function getLastClickedCoordinates():Point
		{
			return lastClickedCoordinates;
		}
		
		public function onClickAction(e:MouseEvent)
		{
			lastClickedCoordinates = new Point(e.localX, e.localY);
			dispatchOnClick(e);
		}
		
		public function addRemoveListener(func:Function)
		{
			removeListeners.push(func);
		}
		
		public function remove()
		{
			dispatchOnRemove();
		}
	}
	
}
