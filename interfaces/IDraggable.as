﻿package  interfaces{
	import flash.geom.Point;
	
	public interface IDraggable {
		function drag(destination:Point):void;
		function onStopDragging():void;
		function GetCoordinates():Point;
	}
	
}
