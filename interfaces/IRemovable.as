﻿package  interfaces{
	
	public interface IRemovable {
		function remove();
		function addRemoveListener(func:Function);
	}
	
}
