﻿package  interfaces{
	import flash.geom.Point;
	import flash.events.MouseEvent;
	
	public interface IClikable {
		function getLastClickedCoordinates():Point;
		function onClickAction(e:MouseEvent);
		function addClicklListener(func:Function);
	}
	
}
